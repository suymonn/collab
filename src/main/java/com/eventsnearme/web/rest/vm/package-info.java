/**
 * View Models used by Spring MVC REST controllers.
 */
package com.eventsnearme.web.rest.vm;

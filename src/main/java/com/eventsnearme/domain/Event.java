package com.eventsnearme.domain;

import com.eventsnearme.domain.enumeration.EventFrequency;
import com.eventsnearme.domain.enumeration.EventType;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import javax.validation.constraints.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * A Event.
 */
@Document(collection = "event")
public class Event extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull(message = "must not be null")
    @Size(min = 4)
    @Field("title")
    private String title;

    @Field("description")
    private String description;

    @Field("picture")
    private byte[] picture;

    @Field("picture_content_type")
    private String pictureContentType;

    @NotNull(message = "must not be null")
    @Field("when")
    private ZonedDateTime when;

    @Field("type")
    private EventType type;

    @Field("frequency")
    private EventFrequency frequency;

    @NotNull(message = "must not be null")
    @Field("location_latitude")
    private Double locationLatitude;

    @NotNull(message = "must not be null")
    @Field("location_longitude")
    private Double locationLongitude;

    @DBRef
    @Field("categories")
    private Set<EventCategory> categories = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Event id(String id) {
        this.id = id;
        return this;
    }

    public String getTitle() {
        return this.title;
    }

    public Event title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public Event description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getPicture() {
        return this.picture;
    }

    public Event picture(byte[] picture) {
        this.picture = picture;
        return this;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public String getPictureContentType() {
        return this.pictureContentType;
    }

    public Event pictureContentType(String pictureContentType) {
        this.pictureContentType = pictureContentType;
        return this;
    }

    public void setPictureContentType(String pictureContentType) {
        this.pictureContentType = pictureContentType;
    }

    public ZonedDateTime getWhen() {
        return this.when;
    }

    public Event when(ZonedDateTime when) {
        this.when = when;
        return this;
    }

    public void setWhen(ZonedDateTime when) {
        this.when = when;
    }

    public EventType getType() {
        return this.type;
    }

    public Event type(EventType type) {
        this.type = type;
        return this;
    }

    public void setType(EventType type) {
        this.type = type;
    }

    public EventFrequency getFrequency() {
        return this.frequency;
    }

    public Event frequency(EventFrequency frequency) {
        this.frequency = frequency;
        return this;
    }

    public void setFrequency(EventFrequency frequency) {
        this.frequency = frequency;
    }

    public Double getLocationLatitude() {
        return this.locationLatitude;
    }

    public Event locationLatitude(Double locationLatitude) {
        this.locationLatitude = locationLatitude;
        return this;
    }

    public void setLocationLatitude(Double locationLatitude) {
        this.locationLatitude = locationLatitude;
    }

    public Double getLocationLongitude() {
        return this.locationLongitude;
    }

    public Event locationLongitude(Double locationLongitude) {
        this.locationLongitude = locationLongitude;
        return this;
    }

    public void setLocationLongitude(Double locationLongitude) {
        this.locationLongitude = locationLongitude;
    }

    public Set<EventCategory> getCategories() {
        return this.categories;
    }

    public Event categories(Set<EventCategory> eventCategories) {
        this.setCategories(eventCategories);
        return this;
    }

    public Event addCategory(EventCategory eventCategory) {
        this.categories.add(eventCategory);
        return this;
    }

    public Event removeCategory(EventCategory eventCategory) {
        this.categories.remove(eventCategory);
        return this;
    }

    public void setCategories(Set<EventCategory> eventCategories) {
        this.categories = eventCategories;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Event)) {
            return false;
        }
        return id != null && id.equals(((Event) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Event{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", description='" + getDescription() + "'" +
            ", pictureContentType='" + getPictureContentType() + "'" +
            ", when='" + getWhen() + "'" +
            ", type='" + getType() + "'" +
            ", frequency='" + getFrequency() + "'" +
            ", locationLatitude=" + getLocationLatitude() +
            ", locationLongitude=" + getLocationLongitude() +
            "}";
    }
}

package com.eventsnearme.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.eventsnearme.domain.EventCategory} entity.
 */
public class EventCategoryDTO implements Serializable {

    private String id;

    @NotNull(message = "must not be null")
    @Size(min = 3)
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EventCategoryDTO)) {
            return false;
        }

        EventCategoryDTO eventCategoryDTO = (EventCategoryDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, eventCategoryDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EventCategoryDTO{" +
            "id='" + getId() + "'" +
            ", name='" + getName() + "'" +
            "}";
    }
}

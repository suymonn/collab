package com.eventsnearme.service.mapper;

import com.eventsnearme.domain.*;
import com.eventsnearme.service.dto.EventCategoryDTO;
import java.util.Set;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link EventCategory} and its DTO {@link EventCategoryDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface EventCategoryMapper extends EntityMapper<EventCategoryDTO, EventCategory> {
    @Named("nameSet")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    Set<EventCategoryDTO> toDtoNameSet(Set<EventCategory> eventCategory);
}

import { Component, NgZone, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';

import { ITEMS_PER_PAGE } from 'app/config/pagination.constants';
import { ParseLinks } from 'app/core/util/parse-links.service';
import { IEvent } from 'app/entities/event/event.model';
import { EventService } from 'app/entities/event/service/event.service';
import { icon, LatLng, latLng, MapOptions, marker, tileLayer } from 'leaflet';
import { IEventCategory } from 'app/entities/event-category/event-category.model';
import { EventCategoryService } from 'app/entities/event-category/service/event-category.service';

@Component({
  selector: 'jhi-event-category',
  templateUrl: './find-event.component.html',
  styleUrls: ['./find-event.scss'],
})
export class FindEventComponent implements OnInit {
  displayedEvents: IEvent[];
  categories: IEventCategory[];
  isLoading = false;
  itemsPerPage: number;
  predicate: string;
  ascending: boolean;
  selectedEvent?: IEvent;

  filterCategoryId = '';
  filterDate?: Date;
  filterFrequency = '';
  filterType = '';

  mapOptions: MapOptions = {
    layers: [
      tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 20,
        attribution: '...',
        noWrap: true,
      }),
    ],
    zoom: 5,
    center: latLng(51.5074, 0.127),
  };

  private map?: L.Map;
  private allEvents: IEvent[];
  private filteredEvents: IEvent[];

  constructor(
    protected eventService: EventService,
    protected eventCategoryService: EventCategoryService,
    protected parseLinks: ParseLinks,
    protected zone: NgZone
  ) {
    this.allEvents = this.displayedEvents = this.filteredEvents = this.categories = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.isLoading = true;

    this.eventService
      .query({
        page: 0,
        size: 2000,
        sort: this.sort(),
      })
      .subscribe((res: HttpResponse<IEvent[]>) => {
        this.isLoading = false;
        this.allEvents = this.filteredEvents = this.displayedEvents = res.body!;
        this.zone.run(() => this.reloadMapLayouts());
      });

    this.eventCategoryService
      .query({
        page: 0,
        size: 1000,
      })
      .subscribe((res: HttpResponse<IEventCategory[]>) => (this.categories = res.body!));
  }

  applyFilter(): void {
    this.filteredEvents = this.allEvents
      .filter(e => !this.filterCategoryId || e.categories?.map(c => c.id).includes(this.filterCategoryId))
      .filter(e => !this.filterType || this.filterType === e.type)
      .filter(e => !this.filterFrequency || this.filterFrequency === e.frequency)
      .filter(
        e =>
          !this.filterDate ||
          (new Date(this.filterDate).getFullYear() === e.when?.toDate().getFullYear() &&
            new Date(this.filterDate).getMonth() === e.when.toDate().getMonth() &&
            new Date(this.filterDate).getDay() === e.when.toDate().getDay())
      );

    this.onMapMove();
    this.reloadMapLayouts();
  }

  ngOnInit(): void {
    this.loadAll();
    navigator.geolocation.getCurrentPosition(position => {
      this.zone.run(() => {
        this.mapOptions.center = latLng(position.coords.latitude, position.coords.longitude);
        this.mapOptions.zoom = 12;
      });
    });
  }

  trackId(index: number, item: IEvent): string {
    return item.id!;
  }

  pretty(categories: IEventCategory[]): string {
    return categories.map(category => `#${category.name!}`).join(' ');
  }

  selectEvent(event: IEvent): void {
    this.zone.run(() => {
      if (event.id === this.selectedEvent?.id) {
        this.selectedEvent = undefined;
      } else {
        this.selectedEvent = event;
        document.getElementById(this.getElementHtmlCardId(event))?.scrollIntoView({ behavior: 'smooth' });
      }
      this.reloadMapLayouts();
    });
  }

  getElementHtmlCardId(event: IEvent): string {
    return `event-${event.id!}`;
  }

  moveMapToEvent(event: IEvent): void {
    this.zone.run(() => (this.mapOptions.center = latLng(event.locationLatitude!, event.locationLongitude!)));
  }

  getCenter(mapOptions: MapOptions): LatLng {
    return latLng(mapOptions.center!);
  }

  onMapMove(): void {
    const bounds = this.map?.getBounds();
    this.displayedEvents = this.filteredEvents.filter(e => bounds?.contains(latLng(e.locationLatitude!, e.locationLongitude!)));
  }

  onMapReady(event: L.Map): void {
    this.map = event;
  }

  protected sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected onMarkerClick(event: any): void {
    const coordinates: LatLng = event.target.getLatLng();
    const foundEvent = this.allEvents
      .filter(e => e.locationLongitude === coordinates.lng)
      .filter(e => e.locationLatitude === coordinates.lat)
      .pop();

    if (foundEvent) {
      this.selectEvent(foundEvent);
    }
  }

  protected reloadMapLayouts(): void {
    this.mapOptions.layers = this.mapOptions.layers?.slice(0, 1);
    this.displayedEvents.forEach(event => {
      this.mapOptions.layers?.push(
        marker(latLng(event.locationLatitude!, event.locationLongitude!), {
          icon: icon({
            iconSize: [15, 24],
            iconUrl: 'content/images/marker.png',
          }),
          title: event.title,
          riseOnHover: true,
          opacity: event.id === this.selectedEvent?.id ? 1 : 0.5,
        }).on('click', this.onMarkerClick.bind(this))
      );
    });
  }
}

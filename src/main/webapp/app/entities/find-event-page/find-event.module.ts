import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { FindEventComponent } from './list/find-event.component';
import { FindEventRoutingModule } from './route/find-event-routing.module';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

@NgModule({
  imports: [SharedModule, FindEventRoutingModule, LeafletModule],
  declarations: [FindEventComponent],
  entryComponents: [],
})
export class FindEventModule {}

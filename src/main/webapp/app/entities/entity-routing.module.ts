import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'event-category',
        data: { pageTitle: 'Admin: Categories' },
        loadChildren: () => import('./event-category/event-category.module').then(m => m.EventCategoryModule),
      },
      {
        path: 'event',
        data: { pageTitle: 'Events' },
        loadChildren: () => import('./event/event.module').then(m => m.EventModule),
      },
      {
        path: 'find',
        data: { pageTitle: 'Find Events' },
        loadChildren: () => import('./find-event-page/find-event.module').then(m => m.FindEventModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}

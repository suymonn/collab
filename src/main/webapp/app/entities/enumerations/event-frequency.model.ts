export enum EventFrequency {
  SINGLE = 'SINGLE',

  DAILY = 'DAILY',

  WEEKLY = 'WEEKLY',

  MONTHLY = 'MONTHLY',

  YEARLY = 'YEARLY',
}

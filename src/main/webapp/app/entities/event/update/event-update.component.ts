import { Component, OnInit, ElementRef } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IEvent, Event } from '../event.model';
import { EventService } from '../service/event.service';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { EventManager, EventWithContent } from 'app/core/util/event-manager.service';
import { DataUtils, FileLoadError } from 'app/core/util/data-util.service';
import { IEventCategory } from 'app/entities/event-category/event-category.model';
import { EventCategoryService } from 'app/entities/event-category/service/event-category.service';
import { icon, latLng, LeafletMouseEvent, MapOptions, marker, tileLayer } from 'leaflet';

@Component({
  selector: 'jhi-event-update',
  templateUrl: './event-update.component.html',
})
export class EventUpdateComponent implements OnInit {
  isSaving = false;

  eventCategoriesSharedCollection: IEventCategory[] = [];

  editForm = this.fb.group({
    id: [],
    title: [null, [Validators.required, Validators.minLength(4)]],
    description: [],
    picture: [],
    pictureContentType: [],
    when: [null, [Validators.required]],
    type: [],
    frequency: [],
    locationLatitude: [null, [Validators.required]],
    locationLongitude: [null, [Validators.required]],
    categories: [],
    createdBy: [],
  });

  mapOptions: MapOptions = {
    layers: [tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 20, attribution: '...' })],
    zoom: 5,
    center: latLng(51.5074, 0.127),
  };

  constructor(
    protected dataUtils: DataUtils,
    protected eventManager: EventManager,
    protected eventService: EventService,
    protected eventCategoryService: EventCategoryService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ event }) => {
      if (event.id === undefined) {
        const today = dayjs().startOf('day');
        event.when = today;
      }

      this.updateForm(event);
      this.loadRelationshipsOptions();
    });
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }

  setFileData(event: any, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe({
      error: (err: FileLoadError) =>
        this.eventManager.broadcast(
          new EventWithContent<AlertError>('eventsNearMeApp.error', { message: err.message })
        ),
    });
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string): void {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null,
    });
    if (idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const event = this.createFromForm();
    if (event.id !== undefined) {
      this.subscribeToSaveResponse(this.eventService.update(event));
    } else {
      this.subscribeToSaveResponse(this.eventService.create(event));
    }
  }

  trackEventCategoryById(index: number, item: IEventCategory): string {
    return item.id!;
  }

  getSelectedEventCategory(option: IEventCategory, selectedVals?: IEventCategory[]): IEventCategory {
    if (selectedVals) {
      for (const selectedVal of selectedVals) {
        if (option.id === selectedVal.id) {
          return selectedVal;
        }
      }
    }
    return option;
  }

  onMapClick(event: LeafletMouseEvent): void {
    this.mapOptions.layers = this.mapOptions.layers?.slice(0, 1);
    this.mapOptions.layers?.push(
      marker(event.latlng, {
        icon: icon({
          iconSize: [25, 41],
          iconUrl: 'content/images/marker.png',
        }),
        title: 'Your event location',
      })
    );

    this.editForm.patchValue({
      locationLatitude: event.latlng.lat,
      locationLongitude: event.latlng.lng,
    });
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEvent>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(event: IEvent): void {
    this.editForm.patchValue({
      id: event.id,
      title: event.title,
      description: event.description,
      picture: event.picture,
      pictureContentType: event.pictureContentType,
      when: event.when ? event.when.format(DATE_TIME_FORMAT) : null,
      type: event.type,
      frequency: event.frequency,
      locationLatitude: event.locationLatitude,
      locationLongitude: event.locationLongitude,
      categories: event.categories,
      createdBy: event.createdBy,
    });

    this.eventCategoriesSharedCollection = this.eventCategoryService.addEventCategoryToCollectionIfMissing(
      this.eventCategoriesSharedCollection,
      ...(event.categories ?? [])
    );

    if (this.mapOptions.layers && this.mapOptions.layers.length <= 1 && event.locationLongitude && event.locationLatitude) {
      this.mapOptions.layers.push(
        marker(latLng(event.locationLatitude, event.locationLongitude), {
          icon: icon({
            iconSize: [25, 41],
            iconUrl: 'content/images/marker.png',
          }),
          title: 'Your event location',
        })
      );
    }
  }

  protected loadRelationshipsOptions(): void {
    this.eventCategoryService
      .query()
      .pipe(map((res: HttpResponse<IEventCategory[]>) => res.body ?? []))
      .pipe(
        map((eventCategories: IEventCategory[]) =>
          this.eventCategoryService.addEventCategoryToCollectionIfMissing(
            eventCategories,
            ...(this.editForm.get('categories')!.value ?? [])
          )
        )
      )
      .subscribe((eventCategories: IEventCategory[]) => (this.eventCategoriesSharedCollection = eventCategories));
  }

  protected createFromForm(): IEvent {
    return {
      ...new Event(),
      id: this.editForm.get(['id'])!.value,
      title: this.editForm.get(['title'])!.value,
      description: this.editForm.get(['description'])!.value,
      pictureContentType: this.editForm.get(['pictureContentType'])!.value,
      picture: this.editForm.get(['picture'])!.value,
      when: this.editForm.get(['when'])!.value ? dayjs(this.editForm.get(['when'])!.value, DATE_TIME_FORMAT) : undefined,
      type: this.editForm.get(['type'])!.value,
      frequency: this.editForm.get(['frequency'])!.value,
      locationLatitude: this.editForm.get(['locationLatitude'])!.value,
      locationLongitude: this.editForm.get(['locationLongitude'])!.value,
      categories: this.editForm.get(['categories'])!.value,
      createdBy: this.editForm.get(['createdBy'])?.value,
    };
  }
}

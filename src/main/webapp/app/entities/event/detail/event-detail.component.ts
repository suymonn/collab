import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IEvent } from '../event.model';
import { DataUtils } from 'app/core/util/data-util.service';
import { icon, latLng, MapOptions, marker, tileLayer } from 'leaflet';
import { AccountService } from 'app/core/auth/account.service';
import { User } from 'app/admin/user-management/user-management.model';
import { Authority } from 'app/config/authority.constants';

@Component({
  selector: 'jhi-event-detail',
  templateUrl: './event-detail.component.html',
})
export class EventDetailComponent implements OnInit {
  event: IEvent | null = null;
  mapOptions: MapOptions = {
    layers: [tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 20, attribution: '...' })],
    zoom: 5,
    center: latLng(51.5074, 0.127),
  };
  private currentUser?: User | null;

  constructor(protected dataUtils: DataUtils, protected activatedRoute: ActivatedRoute, protected accountService: AccountService) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ event }) => {
      this.event = event;
      if (this.event?.locationLatitude && this.event.locationLongitude) {
        this.mapOptions.center = latLng(this.event.locationLatitude, this.event.locationLongitude);
        this.mapOptions.layers?.push(
          marker(this.mapOptions.center, {
            icon: icon({
              iconSize: [25, 41],
              iconUrl: 'content/images/marker.png',
            }),
            title: event.title,
          })
        );
      }
      this.mapOptions.zoom = 12;
    });

    this.accountService.identity(true).subscribe(user => {
      this.currentUser = user;
    });
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }

  previousState(): void {
    window.history.back();
  }

  canEditEvent(): boolean {
    //eslint-disable-next-line @typescript-eslint/prefer-nullish-coalescing
    return this.currentUser?.authorities?.includes(Authority.ADMIN) || this.event?.createdBy === this.currentUser?.login;
  }
}
